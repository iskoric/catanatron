import os

SCRIPT_DIR = f'{os.path.dirname(__file__)}/'
SCRIPT_PATH = os.path.realpath(f'{SCRIPT_DIR}nnp_fitting.py')


def main():
    os.system(f'{SCRIPT_PATH} -l 32 16 4 -s 500000 -b 20 -e 100 -r 0.1 -d 0 -p 3')
    os.system(f'{SCRIPT_PATH} -l 16 8 -s 500000 -b 20 -e 100 -r 0.1 -d 0 -p 3')
    os.system(f'{SCRIPT_PATH} -l 16 8 2 -s 500000 -b 20 -e 100 -r 0.1 -d 0 -p 3')
    os.system(f'{SCRIPT_PATH} -l 64 32 -s 500000 -b 20 -e 100 -r 0.1 -d 0 -p 3')
    os.system(f'{SCRIPT_PATH} -l 64 32 8 -s 500000 -b 20 -e 100 -r 0.1 -d 0 -p 3')
    os.system(f'{SCRIPT_PATH} -l 64 -s 500000 -b 20 -e 100 -r 0.1 -d 0 -p 3')
    os.system(f'{SCRIPT_PATH} -l 32 -s 500000 -b 20 -e 100 -r 0.1 -d 0 -p 3')


if __name__ == '__main__':
    main()
