from __future__ import annotations

import ast
import math
import random

from collections import Counter

from catanatron.models.actions import settlement_possibilities, road_building_possibilities
from catanatron.models.enums import SETTLEMENT, CITY
from catanatron.models.player import Player, Color
from catanatron.state_functions import get_actual_victory_points, get_player_buildings, get_longest_road_length, \
    get_played_dev_cards, get_dev_cards_in_hand, player_deck_to_array, player_num_resource_cards, \
    get_visible_victory_points
from catan_ai.players.util import Expense, Value, Resource, expense_costs, PlayerType


class ValueFunctionPlayer(Player):

    def __init__(self, color, parameter_filepath: str = None, is_bot=True):
        super().__init__(color, is_bot)

        self.expense_multipliers: dict[Expense, int] = {
            Expense.CITY: 100,
            Expense.SETTLEMENT: 50,
            Expense.ROAD: 20,
            Expense.DEV_CARD: 20
        }

        self.resource_multipliers: dict[Resource, int] = {
            Resource.BRICK: 100,
            Resource.ORE: 100,
            Resource.SHEEP: 100,
            Resource.WHEAT: 100,
            Resource.WOOD: 100
        }

        self.value_multipliers: dict[Value, int] = {
            Value.VP: 1500,

            Value.CITIES: 400,
            Value.SETTLEMENTS: 250,
            Value.ROAD_LENGTH: 150,
            Value.KNIGHTS: 80,
            Value.DEV_CARDS: 40,

            Value.RESOURCE_TYPES: 120,

            Value.PORT_VALUE: 100,

            Value.BUILDABLE_NODES: 120,
            Value.BUILDABLE_EDGES: 80,

            Value.DISCARD_PENALTY: -100,

            Value.ENEMY_VP: -200,

            Value.ENEMY_RESOURCE_PRODUCTION: -100,
            Value.ENEMY_RESOURCE_TYPES: -50,

            Value.ENEMY_BUILDABLE_NODES: -100,
            Value.ENEMY_BUILDABLE_EDGES: -50
        }

        if parameter_filepath is not None:
            with open(parameter_filepath, 'r') as file:
                parameters = ast.literal_eval(file.read())
                self.set_parameters(parameters)

        self.player_type = PlayerType.AGGRESSIVE

    def get_parameters(self) -> tuple[int]:
        return tuple(self.expense_multipliers.values()) + \
               tuple(self.resource_multipliers.values()) + \
               tuple(self.value_multipliers.values())

    def set_parameters(self, parameters: tuple[int]):
        i = 0

        for key in self.expense_multipliers.keys():
            self.expense_multipliers[key] = parameters[i]
            i += 1

        for key in self.resource_multipliers.keys():
            self.resource_multipliers[key] = parameters[i]
            i += 1

        for key in self.value_multipliers.keys():
            self.value_multipliers[key] = parameters[i]
            i += 1

    def decide(self, game, playable_actions):
        if len(playable_actions) == 1:
            return playable_actions[0]

        best_value = -math.inf
        best_actions = []

        for action in playable_actions:
            game_copy = game.copy()
            game_copy.execute(action)

            state = game_copy.state
            current_value = self.calculate_value(state)

            if current_value > best_value:
                best_value = current_value
                best_actions = [action]
            elif current_value == best_value:
                best_actions.append(action)

        return random.choice(best_actions)

    def calculate_value(self, state):
        value = 0

        vp = get_actual_victory_points(state, self.color)
        value += vp * self.value_multipliers[Value.VP]

        cities = len(get_player_buildings(state, self.color, CITY))
        value += cities * self.value_multipliers[Value.CITIES]
        settlements = len(get_player_buildings(state, self.color, SETTLEMENT))
        value += settlements * self.value_multipliers[Value.SETTLEMENTS]
        road_length = get_longest_road_length(state, self.color)
        value += road_length * self.value_multipliers[Value.ROAD_LENGTH]
        knights = get_played_dev_cards(state, self.color, 'KNIGHT')
        value += knights * self.value_multipliers[Value.KNIGHTS]
        dev_cards = get_dev_cards_in_hand(state, self.color)
        value += dev_cards * self.value_multipliers[Value.KNIGHTS]

        production = self._get_resource_production(state)

        for resource, multiplier in self.resource_multipliers.items():
            value += production[resource.name] * multiplier
        resource_types = sum(map(lambda x: 1 if x > 0 else 0, production.values()))
        value += resource_types * self.value_multipliers[Value.RESOURCE_TYPES]

        value += self._evaluate_ports(state, production)

        buildable_nodes = len(settlement_possibilities(state, self.color))
        value += buildable_nodes * self.value_multipliers[Value.BUILDABLE_NODES]
        buildable_edges = len(road_building_possibilities(state, self.color))
        value += buildable_edges * self.value_multipliers[Value.BUILDABLE_EDGES]

        value += self._evaluate_hand(state)
        over_discard_limit = max(0, player_num_resource_cards(state, self.color) - state.discard_limit)
        value += over_discard_limit * self.value_multipliers[Value.DISCARD_PENALTY]

        if self.player_type >= PlayerType.AWARE:
            enemy_colors = [color for color in Color if color != self.color]

            value += self._evaluate_enemy_vps(state, enemy_colors)

            enemy_production = self._get_enemy_resource_productions(state, enemy_colors)

            enemy_probabilities = sum(enemy_production.values())
            value += enemy_probabilities * self.value_multipliers[Value.ENEMY_RESOURCE_PRODUCTION]
            enemy_resource_types = sum(map(lambda x: 1 if x > 0 else 0, enemy_production.values()))
            value += enemy_resource_types * self.value_multipliers[Value.ENEMY_RESOURCE_TYPES]

            if self.player_type >= PlayerType.AGGRESSIVE:
                for color in enemy_colors:
                    try:
                        enemy_buildable_nodes = len(settlement_possibilities(state, color))
                        value += enemy_buildable_nodes * self.value_multipliers[Value.ENEMY_BUILDABLE_NODES]
                        enemy_buildable_edges = len(road_building_possibilities(state, color))
                        value += enemy_buildable_edges * self.value_multipliers[Value.ENEMY_BUILDABLE_EDGES]
                    except KeyError:
                        pass

        return value

    def _get_resource_production(self, state):
        settlement_node_ids = get_player_buildings(state, self.color, SETTLEMENT)
        city_node_ids = get_player_buildings(state, self.color, CITY)
        node_production = state.board.map.node_production

        production = dict.fromkeys([resource.name for resource in Resource], 0)

        for node_id in settlement_node_ids:
            for resource, probability in node_production[node_id].items():
                production[resource] += probability

        for node_id in city_node_ids:
            for resource, probability in node_production[node_id].items():
                production[resource] += 2 * probability

        return production

    def _evaluate_ports(self, state, production):
        value = 0

        ports = set(state.board.get_player_port_resources(self.color))
        for port in ports:
            if port is None:
                value += sum(production.values()) / len(production.values())
            else:
                value += production[port]

        return value * self.value_multipliers[Value.PORT_VALUE]

    def _evaluate_hand(self, state):
        value = 0

        hand = player_deck_to_array(state, self.color)
        resource_count = Counter(hand)

        for expense, cost in expense_costs.items():
            current_value = 0
            for resource in cost.keys():
                num_resource = resource_count.get(resource.name, 0)
                current_value += min(1, num_resource / cost[resource])
                # if num_resource < cost[resource]:
                #     current_value -= (cost[resource] - num_resource) / cost[resource]
            value += current_value * self.expense_multipliers[expense]

        return value

    def _evaluate_enemy_vps(self, state, enemy_colors):
        value = 0

        for color in enemy_colors:
            try:
                value += get_visible_victory_points(state, color)
            except KeyError:
                pass

        return value * self.value_multipliers[Value.ENEMY_VP]

    @staticmethod
    def _get_enemy_resource_productions(state, enemy_colors):
        node_production = state.board.map.node_production

        production = dict.fromkeys([resource.name for resource in Resource], 0)

        for color in enemy_colors:
            try:
                settlement_node_ids = get_player_buildings(state, color, SETTLEMENT)
                city_node_ids = get_player_buildings(state, color, CITY)

                for node_id in settlement_node_ids:
                    for resource, probability in node_production[node_id].items():
                        production[resource] += probability

                for node_id in city_node_ids:
                    for resource, probability in node_production[node_id].items():
                        production[resource] += 2 * probability
            except KeyError:
                pass

        return production
