import random

from catanatron.models.player import Player
from catanatron.models.enums import ActionType


class WeightedRandomPlayer(Player):

    def __init__(self, color, is_bot=True):
        super().__init__(color, is_bot)

        self.action_type_weights = {
            ActionType.BUILD_CITY: 1000,
            ActionType.BUILD_SETTLEMENT: 700,
            ActionType.BUILD_ROAD: 300,
            ActionType.BUY_DEVELOPMENT_CARD: 250,
            ActionType.PLAY_KNIGHT_CARD: 50,
            ActionType.PLAY_MONOPOLY: 50,
            ActionType.PLAY_ROAD_BUILDING: 50,
            ActionType.PLAY_YEAR_OF_PLENTY: 50
        }

    def decide(self, game, playable_actions):
        if len(playable_actions) == 1:
            return playable_actions[0]

        weights = list(map(lambda action: self.action_type_weights.get(action.action_type, 1), playable_actions))

        return random.choices(playable_actions, weights, k=1)[0]
