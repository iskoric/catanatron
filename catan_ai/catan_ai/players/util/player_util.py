from __future__ import annotations

from enum import Enum


class PlayerType(Enum):
    DEFENSIVE = 1
    """ Considers only own state, not enemy's """
    AWARE = 2
    """ Considers own state, and only major indicators of enemy's state 
    (VPs and resource production - useful when robbing, taking over longest road...) """
    AGGRESSIVE = 3
    """ Considers own state, and a more detailed summary of enemy's state
    (VPs, resource production, build opportunities, resources in hand...) """

    def __lt__(self, other: PlayerType):
        return self.value < other.value

    def __le__(self, other: PlayerType):
        return self.value <= other.value

    def __eq__(self, other):
        return self.value == other.value

    def __ge__(self, other: PlayerType):
        return self.value >= other.value

    def __gt__(self, other: PlayerType):
        return self.value > other.value
