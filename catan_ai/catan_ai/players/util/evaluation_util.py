from enum import Enum

DICE_PROBABILITIES = {
    2: 1 / 36,
    3: 2 / 36,
    4: 3 / 36,
    5: 4 / 36,
    6: 5 / 36,
    7: 6 / 36,
    8: 5 / 36,
    9: 4 / 36,
    10: 3 / 36,
    11: 2 / 36,
    12: 1 / 36
}


class Resource(Enum):
    BRICK = 'BRICK'
    ORE = 'ORE'
    SHEEP = 'SHEEP'
    WHEAT = 'WHEAT'
    WOOD = 'WOOD'


class Expense(Enum):
    CITY = 'CITY'
    SETTLEMENT = 'SETTLEMENT'
    DEV_CARD = 'DEV_CARD'
    ROAD = 'ROAD'


expense_costs = {
    Expense.CITY: {Resource.ORE: 3, Resource.WHEAT: 2},
    Expense.SETTLEMENT: {Resource.BRICK: 1, Resource.SHEEP: 1, Resource.WHEAT: 1, Resource.WOOD: 1},
    Expense.ROAD: {Resource.BRICK: 1, Resource.WOOD: 1},
    Expense.DEV_CARD: {Resource.ORE: 1, Resource.SHEEP: 1, Resource.WHEAT: 1}
}


class Value(Enum):
    VP = 'VP'
    """ Number of victory points """

    CITIES = 'CITIES'
    """ Number of cities """
    SETTLEMENTS = 'SETTLEMENTS'
    """ Number of settlements """
    ROAD_LENGTH = 'ROAD_LENGTH'
    """ Maximum road length """
    KNIGHTS = 'KNIGHTS'
    """ Number of played knights """
    DEV_CARDS = 'DEV_CARDS'
    """ Number of hidden dev cards """

    RESOURCE_PRODUCTION = 'RESOURCE_PRODUCTION'
    """ Resource production value """
    RESOURCE_TYPES = 'RESOURCE_TYPES'
    """ Number of produced resource types """

    BRICK_PROBABILITY = 'BRICK_PROBABILITY'
    """ Brick production probability """
    ORE_PROBABILITY = 'ORE_PROBABILITY'
    """ Ore production probability """
    SHEEP_PROBABILITY = 'SHEEP_PROBABILITY'
    """ Sheep production probability """
    WHEAT_PROBABILITY = 'WHEAT_PROBABILITY'
    """ Wheat production probability """
    WOOD_PROBABILITY = 'WOOD_PROBABILITY'
    """ Wood production probability """

    PORT_VALUE = 'PORT_VALUE'
    """ Port possession value """

    GENERAL_PORTS = 'GENERAL_PORTS'
    """ Number of general ports """
    BRICK_PORTS = 'BRICK_PORTS'
    """ Number of brick ports """
    ORE_PORTS = 'ORE_PORTS'
    """ Number of ore ports """
    SHEEP_PORTS = 'SHEEP_PORTS'
    """ Number of sheep ports """
    WHEAT_PORTS = 'WHEAT_PORTS'
    """ Number of wheat ports """
    WOOD_PORTS = 'WOOD_PORTS'
    """ Number of wood ports """

    BUILDABLE_NODES = 'BUILDABLE_NODES'
    """ Number of buildable nodes """
    BUILDABLE_EDGES = 'BUILDABLE_EDGES'
    """ Number of buildable edges """

    HAND_VALUE = 'HAND_VALUE'
    """ Value of resources in hand """
    DISCARD_PENALTY = 'DISCARD_PENALTY'
    """ Penalty for holding cards over the discard limit """

    BRICKS = 'BRICKS'
    """ Amount of bricks in hand """
    ORE = 'ORE'
    """ Amount of ore in hand """
    SHEEP = 'SHEEP'
    """ Amount of sheep in hand """
    WHEAT = 'WHEAT'
    """ Amount of wheat in hand """
    WOOD = 'WOOD'
    """ Amount of wood in hand """

    ENEMY_VP = 'ENEMY_VP'
    """ Enemies' visible victory points """

    ENEMY_RESOURCE_PRODUCTION = 'ENEMY_RESOURCE_PRODUCTION'
    """ Enemies' resource production value """
    ENEMY_RESOURCE_TYPES = 'ENEMY_RESOURCE_TYPES'
    """ Number of produced resource types by enemy """

    ENEMY_BUILDABLE_NODES = 'ENEMY_BUILDABLE_NODES'
    """ Number of buildable nodes for enemy """
    ENEMY_BUILDABLE_EDGES = 'ENEMY_BUILDABLE_EDGES'
    """ Number of buildable edges for enemy """
