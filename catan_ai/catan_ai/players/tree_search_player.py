from __future__ import annotations

import ast
import math
import random

from collections import Counter

from catanatron import Game
from catanatron.game import TURNS_LIMIT
from catanatron.models.actions import settlement_possibilities, road_building_possibilities
from catanatron.models.enums import SETTLEMENT, CITY, Action, ActionType, DEVELOPMENT_CARDS, RESOURCES
from catanatron.models.player import Player, Color
from catanatron.state_functions import get_actual_victory_points, get_player_buildings, get_longest_road_length, \
    get_played_dev_cards, get_dev_cards_in_hand, player_deck_to_array, player_num_resource_cards, \
    get_visible_victory_points
from catan_ai.players.util import Expense, Value, Resource, expense_costs, PlayerType, DICE_PROBABILITIES


class StateNode:

    def __init__(self, game: Game, probability: float = 1):
        self.game = game
        self.probability = probability
        self.value = None
        self.children = []

    def __str__(self):
        return f'[Probability: {self.probability}, Value: {self.value}]'


class ActionNode:

    def __init__(self, action: Action | None):
        self.action = action
        self.value = None
        self.children = []

    def __str__(self):
        return f'[Action: {self.action}, Value: {self.value}]'


class TreeSearchPlayer(Player):

    def __init__(self, color, depth: int = 3, parameter_filepath: str = None, is_bot=True):
        super().__init__(color, is_bot)

        self.expense_multipliers: dict[Expense, int] = {
            Expense.CITY: 100,
            Expense.SETTLEMENT: 50,
            Expense.ROAD: 20,
            Expense.DEV_CARD: 20
        }

        self.resource_multipliers: dict[Resource, int] = {
            Resource.BRICK: 100,
            Resource.ORE: 100,
            Resource.SHEEP: 100,
            Resource.WHEAT: 100,
            Resource.WOOD: 100
        }

        self.value_multipliers: dict[Value, int] = {
            Value.VP: 1500,

            Value.CITIES: 400,
            Value.SETTLEMENTS: 250,
            Value.ROAD_LENGTH: 150,
            Value.KNIGHTS: 80,
            Value.DEV_CARDS: 40,

            Value.RESOURCE_TYPES: 120,

            Value.PORT_VALUE: 100,

            Value.BUILDABLE_NODES: 120,
            Value.BUILDABLE_EDGES: 80,

            Value.DISCARD_PENALTY: -100,

            Value.ENEMY_VP: -200,

            Value.ENEMY_RESOURCE_PRODUCTION: -100,
            Value.ENEMY_RESOURCE_TYPES: -50,

            Value.ENEMY_BUILDABLE_NODES: -100,
            Value.ENEMY_BUILDABLE_EDGES: -50
        }

        if parameter_filepath is not None:
            with open(parameter_filepath, 'r') as file:
                parameters = ast.literal_eval(file.read())
                self.set_parameters(parameters)

        self.player_type = PlayerType.AGGRESSIVE

        self.depth = int(depth)
        self.probability_cutoff = 0

    def get_parameters(self) -> tuple[int]:
        return tuple(self.expense_multipliers.values()) + \
               tuple(self.resource_multipliers.values()) + \
               tuple(self.value_multipliers.values())

    def set_parameters(self, parameters: tuple[int]):
        i = 0

        for key in self.expense_multipliers.keys():
            self.expense_multipliers[key] = parameters[i]
            i += 1

        for key in self.resource_multipliers.keys():
            self.resource_multipliers[key] = parameters[i]
            i += 1

        for key in self.value_multipliers.keys():
            self.value_multipliers[key] = parameters[i]
            i += 1

    def decide(self, game, playable_actions):
        if len(playable_actions) == 1:
            return playable_actions[0]

        node = StateNode(game.copy())
        result = self.search(node, self.depth, -math.inf, math.inf)

        return result.action

    def search(self, parent: StateNode, depth: int, alpha: float, beta: float) -> ActionNode:
        if depth == 0 or TreeSearchPlayer._is_game_over(parent.game):
            result = ActionNode(None)
            result.value = self.calculate_value(parent.game.state)
            parent.children.append(result)
            return result

        is_maximizing_player = parent.game.state.current_color() == self.color

        if is_maximizing_player:
            parent.value = -math.inf
            best_nodes = []

            for action in parent.game.state.playable_actions:
                action_node = ActionNode(action)
                action_node.value = 0
                parent.children.append(action_node)

                state_nodes = self._expand_node(parent, action_node)
                for state_node in state_nodes:
                    result = self.search(state_node, depth - 1, alpha, beta)
                    action_node.value += result.value * state_node.probability

                if action_node.value > parent.value:
                    parent.value = action_node.value
                    best_nodes = [action_node]
                elif action_node.value == parent.value:
                    best_nodes.append(action_node)

                alpha = max(alpha, parent.value)

                if alpha >= beta:
                    break

            return random.choice(best_nodes)
        else:
            parent.value = math.inf
            best_nodes = []

            for action in parent.game.state.playable_actions:
                action_node = ActionNode(action)
                action_node.value = 0
                parent.children.append(action_node)

                state_nodes = self._expand_node(parent, action_node)
                for state_node in state_nodes:
                    result = self.search(state_node, depth - 1, alpha, beta)
                    action_node.value += result.value * state_node.probability

                if action_node.value < parent.value:
                    parent.value = action_node.value
                    best_nodes = [action_node]
                elif action_node.value == parent.value:
                    best_nodes.append(action_node)

                beta = min(beta, parent.value)

                if beta <= alpha:
                    break

            return random.choice(best_nodes)

    def calculate_value(self, state):
        value = 0

        vp = get_actual_victory_points(state, self.color)
        value += vp * self.value_multipliers[Value.VP]

        cities = len(get_player_buildings(state, self.color, CITY))
        value += cities * self.value_multipliers[Value.CITIES]
        settlements = len(get_player_buildings(state, self.color, SETTLEMENT))
        value += settlements * self.value_multipliers[Value.SETTLEMENTS]
        road_length = get_longest_road_length(state, self.color)
        value += road_length * self.value_multipliers[Value.ROAD_LENGTH]
        knights = get_played_dev_cards(state, self.color, 'KNIGHT')
        value += knights * self.value_multipliers[Value.KNIGHTS]
        dev_cards = get_dev_cards_in_hand(state, self.color)
        value += dev_cards * self.value_multipliers[Value.KNIGHTS]

        production = self._get_resource_production(state)

        for resource, multiplier in self.resource_multipliers.items():
            value += production[resource.name] * multiplier
        resource_types = sum(map(lambda x: 1 if x > 0 else 0, production.values()))
        value += resource_types * self.value_multipliers[Value.RESOURCE_TYPES]

        value += self._evaluate_ports(state, production)

        buildable_nodes = len(settlement_possibilities(state, self.color))
        value += buildable_nodes * self.value_multipliers[Value.BUILDABLE_NODES]
        buildable_edges = len(road_building_possibilities(state, self.color))
        value += buildable_edges * self.value_multipliers[Value.BUILDABLE_EDGES]

        value += self._evaluate_hand(state)
        over_discard_limit = max(0, player_num_resource_cards(state, self.color) - state.discard_limit)
        value += over_discard_limit * self.value_multipliers[Value.DISCARD_PENALTY]

        if self.player_type >= PlayerType.AWARE:
            enemy_colors = [color for color in Color if color != self.color]

            value += self._evaluate_enemy_vps(state, enemy_colors)

            enemy_production = self._get_enemy_resource_productions(state, enemy_colors)

            enemy_probabilities = sum(enemy_production.values())
            value += enemy_probabilities * self.value_multipliers[Value.ENEMY_RESOURCE_PRODUCTION]
            enemy_resource_types = sum(map(lambda x: 1 if x > 0 else 0, enemy_production.values()))
            value += enemy_resource_types * self.value_multipliers[Value.ENEMY_RESOURCE_TYPES]

            if self.player_type >= PlayerType.AGGRESSIVE:
                for color in enemy_colors:
                    try:
                        enemy_buildable_nodes = len(settlement_possibilities(state, color))
                        value += enemy_buildable_nodes * self.value_multipliers[Value.ENEMY_BUILDABLE_NODES]
                        enemy_buildable_edges = len(road_building_possibilities(state, color))
                        value += enemy_buildable_edges * self.value_multipliers[Value.ENEMY_BUILDABLE_EDGES]
                    except KeyError:
                        pass

        return value

    def _get_resource_production(self, state):
        settlement_node_ids = get_player_buildings(state, self.color, SETTLEMENT)
        city_node_ids = get_player_buildings(state, self.color, CITY)
        node_production = state.board.map.node_production

        production = dict.fromkeys([resource.name for resource in Resource], 0)

        for node_id in settlement_node_ids:
            for resource, probability in node_production[node_id].items():
                production[resource] += probability

        for node_id in city_node_ids:
            for resource, probability in node_production[node_id].items():
                production[resource] += 2 * probability

        return production

    def _evaluate_ports(self, state, production):
        value = 0

        ports = state.board.get_player_port_resources(self.color)
        for port in ports:
            if port is None:
                value += sum(production.values()) / len(production.values())
            else:
                value += production[port]

        return value * self.value_multipliers[Value.PORT_VALUE]

    def _evaluate_hand(self, state):
        value = 0

        hand = player_deck_to_array(state, self.color)
        resource_count = Counter(hand)

        for expense, cost in expense_costs.items():
            current_value = 0
            for resource in cost.keys():
                num_resource = resource_count.get(resource.name, 0)
                current_value += min(1, num_resource / cost[resource])
                # if num_resource < cost[resource]:
                #     current_value -= (cost[resource] - num_resource) / cost[resource]
            value += current_value * self.expense_multipliers[expense]

        return value

    def _evaluate_enemy_vps(self, state, enemy_colors):
        value = 0

        for color in enemy_colors:
            try:
                value += get_visible_victory_points(state, color)
            except KeyError:
                pass

        return value * self.value_multipliers[Value.ENEMY_VP]

    @staticmethod
    def _get_enemy_resource_productions(state, enemy_colors):
        node_production = state.board.map.node_production

        production = dict.fromkeys([resource.name for resource in Resource], 0)

        for color in enemy_colors:
            try:
                settlement_node_ids = get_player_buildings(state, color, SETTLEMENT)
                city_node_ids = get_player_buildings(state, color, CITY)

                for node_id in settlement_node_ids:
                    for resource, probability in node_production[node_id].items():
                        production[resource] += probability

                for node_id in city_node_ids:
                    for resource, probability in node_production[node_id].items():
                        production[resource] += 2 * probability
            except KeyError:
                pass

        return production

    @staticmethod
    def _is_game_over(game: Game):
        return game.winning_color() is not None or game.state.current_turn_index >= TURNS_LIMIT

    def _expand_node(self, parent: StateNode, action_node: ActionNode) -> list[StateNode]:
        game = parent.game
        action = action_node.action

        if action.action_type == ActionType.ROLL:
            filtered_rolls = dict(
                filter(lambda item: item[1] >= self.probability_cutoff, DICE_PROBABILITIES.items())
            )
            for roll, probability in filtered_rolls.items():
                dice = (math.floor(roll / 2), math.ceil(roll / 2))
                deterministic_action = Action(action.color, action.action_type, dice)
                game_copy = game.copy()
                game_copy.execute(deterministic_action, validate_action=False)
                action_node.children.append(StateNode(game_copy, probability))
            excess_probability = 1 - sum(filtered_rolls.values())
            if excess_probability > 0 and len(action_node.children) > 0:
                for state_node in action_node.children:
                    state_node.probability += excess_probability / len(action_node.children)
        elif action.action_type == ActionType.BUY_DEVELOPMENT_CARD:
            excess_probability = 0
            for dev_card in DEVELOPMENT_CARDS:
                deterministic_action = Action(action.color, action.action_type, dev_card)
                game_copy = game.copy()
                try:
                    game_copy.execute(deterministic_action, validate_action=False)
                    action_node.children.append(StateNode(game_copy, 1 / len(DEVELOPMENT_CARDS)))
                except Exception:
                    # Development card is not available
                    # Player shouldn't know that some actions might fail,
                    # but disregarding this would cause the same state being evaluated multiple times
                    excess_probability += 1 / len(DEVELOPMENT_CARDS)
            if excess_probability > 0 and len(action_node.children) > 0:
                for state_node in action_node.children:
                    state_node.probability += excess_probability / len(action_node.children)
        elif action.action_type == ActionType.MOVE_ROBBER:
            coordinate, robbed_color, _ = action.value
            if robbed_color is None:
                game_copy = game.copy()
                game_copy.execute(action, validate_action=False)
                action_node.children.append(StateNode(game_copy))
            else:
                excess_probability = 0
                for resource in RESOURCES:
                    action_values = (coordinate, robbed_color, resource)
                    deterministic_action = Action(action.color, action.action_type, action_values)
                    game_copy = game.copy()
                    try:
                        game_copy.execute(deterministic_action, validate_action=False)
                        action_node.children.append(StateNode(game_copy, 1 / len(RESOURCES)))
                    except Exception:
                        # Robbed player doesn't have this resource
                        # Player shouldn't know that some actions might fail,
                        # but disregarding this would cause the same state being evaluated multiple times
                        excess_probability += 1 / len(RESOURCES)
                if excess_probability > 0 and len(action_node.children) > 0:
                    for state_node in action_node.children:
                        state_node.probability += excess_probability / len(action_node.children)
        else:
            game_copy = game.copy()
            game_copy.execute(action, validate_action=False)
            action_node.children.append(StateNode(game_copy))

        return action_node.children
