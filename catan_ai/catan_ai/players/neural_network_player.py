from __future__ import annotations

import ast
import random
from collections import Counter

import numpy as np
import tensorflow as tf

from catan_ai.players.util import Resource
from catanatron.models.actions import settlement_possibilities, road_building_possibilities
from catanatron.models.enums import SETTLEMENT, CITY
from catanatron.models.player import Player, Color
from catanatron.state_functions import get_actual_victory_points, get_player_buildings, get_longest_road_length, \
    get_played_dev_cards, get_dev_cards_in_hand, player_deck_to_array, get_visible_victory_points

np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)


class NeuralNetworkPlayer(Player):

    input_size = 48

    def __init__(self, color, layers: tuple[int, ...] | str = (16,), parameter_filepath: str = None, is_bot=True):
        super().__init__(color, is_bot)

        layers = layers if isinstance(layers, tuple) else ast.literal_eval(layers)

        if len(layers) < 1:
            raise ValueError('Layers must define at least one hidden layer size!')
        self.layers = layers

        model = self.create_model()
        self.weights = model.get_weights()

        self.model = NeuralNetworkPlayer.convert_model(model)

        if parameter_filepath is not None:
            with open(parameter_filepath, 'r') as file:
                parameters = ast.literal_eval(file.read())
                self.set_parameters(parameters)

    def get_parameters(self) -> tuple[float]:
        return tuple(np.concatenate([np.reshape(array, -1) for array in self.weights]))

    def set_parameters(self, parameters: tuple[float]):
        index = 0
        weights = []

        for layer in np.reshape(self.weights, (-1, 2)):
            w = layer[0]
            weights.append(np.array(parameters[index:index + w.size]).reshape(w.shape))
            index += w.size

            b = layer[1]
            weights.append(np.array(parameters[index:index + b.size]).reshape(b.shape))
            index += b.size

        self.weights = weights

        model = self.create_model()
        model.set_weights(weights)

        self.model = NeuralNetworkPlayer.convert_model(model)

    def create_model(self, dropout: float = 0):
        layers = [tf.keras.layers.InputLayer(input_shape=NeuralNetworkPlayer.input_size)]

        initializer = tf.keras.initializers.RandomUniform(minval=-10, maxval=10)

        for size in self.layers:
            layers.append(tf.keras.layers.Dense(size, activation='relu', kernel_initializer=initializer))
            if dropout > 0:
                layers.append(tf.keras.layers.Dropout(dropout))

        layers.append(tf.keras.layers.Dense(1, activation='linear', kernel_initializer=initializer))

        model = tf.keras.models.Sequential(layers=layers)

        return model

    @staticmethod
    def convert_model(model):
        converter = tf.lite.TFLiteConverter.from_keras_model(model)
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
        return converter.convert()

    @staticmethod
    def create_interpreter(model):
        return tf.lite.Interpreter(model_content=model)

    @staticmethod
    def predict(interpreter, x):
        input_details = interpreter.get_input_details()[0]
        output_details = interpreter.get_output_details()[0]

        interpreter.resize_tensor_input(input_details['index'], [x.shape[0], input_details['shape'][1]])
        interpreter.allocate_tensors()

        interpreter.set_tensor(input_details['index'], x)
        interpreter.invoke()
        return interpreter.get_tensor(output_details['index'])

    def decide(self, game, playable_actions):
        if len(playable_actions) == 1:
            return playable_actions[0]

        x = []
        for action in playable_actions:
            game_copy = game.copy()
            game_copy.execute(action)

            state = game_copy.state
            x.append(NeuralNetworkPlayer.create_input(state, self.color))

        interpreter = self.create_interpreter(self.model)

        y = NeuralNetworkPlayer.predict(interpreter, np.array(x, dtype=np.float32))

        y = np.reshape(y, -1)
        best_value = np.max(y)
        best_actions = [playable_actions[i] for i in np.argwhere(y == best_value).reshape(-1)]

        return random.choice(best_actions)

    @staticmethod
    def create_input(state, color):
        production = NeuralNetworkPlayer._get_resource_production(state, color)
        ports = set(state.board.get_player_port_resources(color))
        hand = NeuralNetworkPlayer._get_resources_in_hand(state, color)

        enemy_colors = [c for c in Color if c != color]
        enemy_vps = NeuralNetworkPlayer._get_enemy_vps(state, enemy_colors)
        enemy_productions = NeuralNetworkPlayer._get_enemy_resource_productions(state, enemy_colors)
        enemy_buildable_nodes = NeuralNetworkPlayer._get_enemy_buildable_nodes(state, enemy_colors)
        enemy_buildable_edges = NeuralNetworkPlayer._get_enemy_buildable_edges(state, enemy_colors)

        return np.array([
            get_actual_victory_points(state, color),

            len(get_player_buildings(state, color, CITY)),
            len(get_player_buildings(state, color, SETTLEMENT)),
            get_longest_road_length(state, color),
            get_played_dev_cards(state, color, 'KNIGHT'),
            get_dev_cards_in_hand(state, color),

            production[Resource.BRICK.name],
            production[Resource.ORE.name],
            production[Resource.SHEEP.name],
            production[Resource.WHEAT.name],
            production[Resource.WOOD.name],

            1 if None in ports else 0,
            1 if Resource.BRICK.name in ports else 0,
            1 if Resource.ORE.name in ports else 0,
            1 if Resource.SHEEP.name in ports else 0,
            1 if Resource.WHEAT.name in ports else 0,
            1 if Resource.WOOD.name in ports else 0,

            len(settlement_possibilities(state, color)),
            len(road_building_possibilities(state, color)),

            hand[Resource.BRICK.name],
            hand[Resource.ORE.name],
            hand[Resource.SHEEP.name],
            hand[Resource.WHEAT.name],
            hand[Resource.WOOD.name],

            enemy_vps[0],
            enemy_vps[1],
            enemy_vps[2],

            enemy_productions[0][Resource.BRICK.name],
            enemy_productions[0][Resource.ORE.name],
            enemy_productions[0][Resource.SHEEP.name],
            enemy_productions[0][Resource.WHEAT.name],
            enemy_productions[0][Resource.WOOD.name],

            enemy_productions[1][Resource.BRICK.name],
            enemy_productions[1][Resource.ORE.name],
            enemy_productions[1][Resource.SHEEP.name],
            enemy_productions[1][Resource.WHEAT.name],
            enemy_productions[1][Resource.WOOD.name],

            enemy_productions[2][Resource.BRICK.name],
            enemy_productions[2][Resource.ORE.name],
            enemy_productions[2][Resource.SHEEP.name],
            enemy_productions[2][Resource.WHEAT.name],
            enemy_productions[2][Resource.WOOD.name],

            enemy_buildable_nodes[0],
            enemy_buildable_nodes[1],
            enemy_buildable_nodes[2],

            enemy_buildable_edges[0],
            enemy_buildable_edges[1],
            enemy_buildable_edges[2]
        ])

    @staticmethod
    def _get_resource_production(state, color):
        settlement_node_ids = get_player_buildings(state, color, SETTLEMENT)
        city_node_ids = get_player_buildings(state, color, CITY)
        node_production = state.board.map.node_production

        production = dict.fromkeys([resource.name for resource in Resource], 0)

        for node_id in settlement_node_ids:
            for resource, probability in node_production[node_id].items():
                production[resource] += probability

        for node_id in city_node_ids:
            for resource, probability in node_production[node_id].items():
                production[resource] += 2 * probability

        return production

    @staticmethod
    def _get_resources_in_hand(state, color):
        hand = player_deck_to_array(state, color)
        return Counter(hand)

    @staticmethod
    def _get_enemy_vps(state, enemy_colors):
        enemy_vps = []

        for color in enemy_colors:
            try:
                enemy_vps.append(get_visible_victory_points(state, color))
            except KeyError:
                enemy_vps.append(0)

        return enemy_vps

    @staticmethod
    def _get_enemy_resource_productions(state, enemy_colors):
        enemy_productions = []

        node_production = state.board.map.node_production

        for color in enemy_colors:
            production = dict.fromkeys([resource.name for resource in Resource], 0)

            try:
                settlement_node_ids = get_player_buildings(state, color, SETTLEMENT)
                city_node_ids = get_player_buildings(state, color, CITY)

                for node_id in settlement_node_ids:
                    for resource, probability in node_production[node_id].items():
                        production[resource] += probability

                for node_id in city_node_ids:
                    for resource, probability in node_production[node_id].items():
                        production[resource] += 2 * probability
            except KeyError:
                pass

            enemy_productions.append(production)

        return enemy_productions

    @staticmethod
    def _get_enemy_buildable_nodes(state, enemy_colors):
        enemy_buildable_nodes = []

        for color in enemy_colors:
            try:
                enemy_buildable_nodes.append(len(settlement_possibilities(state, color)))
            except KeyError:
                enemy_buildable_nodes.append(0)

        return enemy_buildable_nodes

    @staticmethod
    def _get_enemy_buildable_edges(state, enemy_colors):
        enemy_buildable_edges = []

        for color in enemy_colors:
            try:
                enemy_buildable_edges.append(len(settlement_possibilities(state, color)))
            except KeyError:
                enemy_buildable_edges.append(0)

        return enemy_buildable_edges
