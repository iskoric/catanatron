from __future__ import annotations

import random
from catanatron.models.player import Player
from catanatron.state_functions import get_actual_victory_points


class GreedyVpPlayer(Player):

    def decide(self, game, playable_actions):
        if len(playable_actions) == 1:
            return playable_actions[0]

        best_vp = 0
        best_actions = []

        for action in playable_actions:
            game_copy = game.copy()
            game_copy.execute(action)

            state = game_copy.state
            current_vp = get_actual_victory_points(state, self.color)

            if current_vp > best_vp:
                best_vp = current_vp
                best_actions = [action]
            elif current_vp == best_vp:
                best_actions.append(action)

        return random.choice(best_actions)
