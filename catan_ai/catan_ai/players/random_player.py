import random

from catanatron.models.player import Player


class RandomPlayer(Player):

    def decide(self, game, playable_actions):
        return random.choice(playable_actions)
