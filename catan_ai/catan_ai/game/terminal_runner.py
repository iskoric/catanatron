from argparse import ArgumentParser, Namespace, Action, ArgumentTypeError, RawTextHelpFormatter

from catan_ai.game.play import play_batch
from catanatron_experimental.play import Color, CLI_PLAYERS


def required_length(min_len, max_len):
    class RequiredLength(Action):
        def __call__(self, parser, args, values, option_string=None):
            if not min_len <= len(values) <= max_len:
                raise ArgumentTypeError(
                    f'argument {"/".join(self.option_strings)} requires between {min_len} and {max_len} values'
                )
            setattr(args, self.dest, values)

    return RequiredLength


def parse_arguments() -> Namespace:
    parser = ArgumentParser(
        description='''
Simulates batch of Catanatron games with given player types, and prints statistics.
Requires a combination of 2-4 players of the following types:
    R        Default Random Player
    W        Default Weighted Random Player
    VP       Default VP Player
    F        Default Value Function Player
    AB       Default Alpha-Beta Player (evaluates value function several turns ahead using alpha-beta pruning)
    IS-R     Random Player by Ivan Skorić
    IS-WR    Weighted Random Player by Ivan Skorić
    IS-VP    Greedy VP Player by Ivan Skorić
    IS-VF    Value Function Player by Ivan Skorić
    IS-NN    Neural Network Player by Ivan Skorić
    IS-TS    Tree Search Player by Ivan Skorić
        ''',
        formatter_class=RawTextHelpFormatter
    )

    parser.add_argument('-n', '--number-of-games', help='Number of games to simulate', type=int, default=1)
    parser.add_argument(
        '-p', '--players',
        help="Between 2 and 4 player codes, with optional player parameters separated by ';'\n"
             "e.g. [-p PLAYER1;opt_param_1;opt_param2 PLAYER2 PLAYER3;opt_param_1 PLAYER4]",
        nargs='*', default=['IS-R', 'IS-WR', 'IS-VP', 'IS-VF'],
        action=required_length(2, 4)
    )

    return parser.parse_args()


def main():
    args = parse_arguments()

    player_keys = args.players
    players = []
    colors = [c for c in Color]
    for i, key in enumerate(player_keys):
        parts = key.split(';')
        code = parts[0]
        for cli_player in CLI_PLAYERS:
            if cli_player.code == code:
                params = [colors[i]] + parts[1:]
                player = cli_player.import_fn(*params)
                players.append(player)
                break

    play_batch(args.number_of_games, players)


if __name__ == '__main__':
    main()
