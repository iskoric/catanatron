class Unit:

    def __init__(self, parameters: tuple[int | float, ...], first_iteration: int = 1) -> None:
        self.parameters = parameters

        self.first_iteration = first_iteration
        self.last_iteration: int | None = None

        self.age = 1

        self.tournaments_played = 0
        self.games_won = 0
        self.tournaments_won = 0
        self.tournaments_dominated = 0

        self.games_won_per_iteration = 0
        self.tournaments_won_per_iteration = 0

    def calculate_age(self, current_iteration: int | None = None) -> int:
        if self.last_iteration is not None:
            self.age = self.last_iteration - self.first_iteration + 1
        elif current_iteration is not None:
            self.age = current_iteration - self.first_iteration + 1
        else:
            raise ValueError('Current iteration must be known to determine age!')

        return self.age

    def calculate_games_won_per_iteration(self, current_iteration: int | None = None) -> float:
        self.games_won_per_iteration = self.games_won / self.calculate_age(current_iteration)
        return self.games_won_per_iteration

    def calculate_tournaments_won_per_iteration(self, current_iteration: int | None = None) -> float:
        self.tournaments_won_per_iteration = self.tournaments_won / self.calculate_age(current_iteration)
        return self.tournaments_won_per_iteration

    def update(self, current_iteration: int):
        self.calculate_games_won_per_iteration(current_iteration)
        self.calculate_tournaments_won_per_iteration(current_iteration)

    def retire(self, current_iteration: int):
        self.last_iteration = current_iteration
        self.calculate_games_won_per_iteration()
        self.calculate_tournaments_won_per_iteration()
