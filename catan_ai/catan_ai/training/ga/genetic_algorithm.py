import random
from typing import Callable

from catanatron import Color, Player
from catan_ai.game.play import play_batch
from catan_ai.training.ga.unit import Unit
from catan_ai.training.ga.crossover import Crossover
from catan_ai.training.ga.mutation import Mutation


class GeneticAlgorithm:

    def __init__(self, population_size: int, parameter_noise: int | float,
                 iterations: int, tournaments: int, tournament_length: int,
                 mutation_chance: float, mutation_strength: int | float, early_stop_patience: int) -> None:
        self.population_size = population_size
        self.parameter_noise = parameter_noise

        self.iterations = iterations

        self.tournaments = tournaments
        self.tournament_length = tournament_length

        self.mutation_chance = mutation_chance
        self.mutation_strength = mutation_strength

        self.early_stop_patience = early_stop_patience
        self.early_stop_count = 0

        self.population = []
        self.wins = dict()

        self.generate_unit: Callable[[int | float], tuple[int | float, ...]] | None = None
        self.create_player: Callable[[tuple[int | float, ...], Color], Player] | None = None

    def run(self) -> None:
        if self.generate_unit is None or self.create_player is None:
            raise ValueError('Unit and player factory functions must be set!')

        self._generate_population()
        self.wins.clear()

        for iteration in range(1, self.iterations + 1):
            print(f'Iteration {iteration}...')

            for tournament in range(1, self.tournaments + 1):
                print(f'\tTournament {tournament}...')

                while True:
                    sample = self._take_sample(self.population)
                    new_sample = self._run_tournament(sample)
                    if new_sample is None:
                        self.population.extend(sample)
                    else:
                        self.population.extend(new_sample)
                        break

                winner = new_sample[0]
                self.wins[winner] = self.wins.get(winner, 0) + 1

                print(f'\t\tFinished tournament!')

            count = 0
            winners = list(self.wins.keys())
            for winner in winners:
                if winner not in self.population:
                    del self.wins[winner]
                    count += 1

            print(f'Removed {count} previous winners from population, {len(self.wins)} winners remaining')

            best_unit = max(self.wins, key=self.wins.get)
            print(f'Best unit in iteration {iteration}:')
            print(f'\t{best_unit}')
            print(f'\tWon {self.wins[best_unit]} tournaments')

            print()

            if count == 0:
                self.early_stop_count += 1
            elif self.early_stop_count > 0:
                self.early_stop_count = 0

            if 0 < self.early_stop_patience <= self.early_stop_count:
                break

    def _generate_population(self) -> None:
        self.population.clear()

        for _ in range(self.population_size - 1):
            self.population.append(self.generate_unit(self.parameter_noise))

        self.population.append(self.generate_unit(0))

    def _run_tournament(self, sample: list[tuple[int | float, ...]]) -> list[tuple[int | float, ...]] | None:
        colors = [c for c in Color]
        players = {colors[i]: self.create_player(sample[i], colors[i]) for i in range(len(colors))}

        wins, _, _ = play_batch(self.tournament_length, list(players.values()), quiet=True)

        winners = list(wins.keys())
        if len(winners) < len(sample):
            for color in colors:
                if color not in winners:
                    wins[color] = 0

        print(f'\t\tWin distribution: {wins}')

        finished_games = sum(wins.values())
        if finished_games < self.tournament_length:
            print(f'\t\tNot all games were finished: {finished_games}/{self.tournament_length}')
        if finished_games < self.tournament_length / 2:
            print(f'\t\tRestarting tournament with new sample...')
            return None

        sorted_colors = sorted(wins, key=wins.get, reverse=True)

        sample = [sample[colors.index(color)] for color in sorted_colors]

        if wins[sorted_colors[0]] > self.tournament_length / 2:
            print(f'\t\tPlayer {sorted_colors[0]} dominated the tournament:')
            print(f'\t\t\t{sample[0]}')

        sample[2] = Crossover.arithmetic_crossover(sample[0], sample[1])
        sample[2] = Mutation.mutate(sample[2], self.mutation_chance, self.mutation_strength)

        sample[3] = Crossover.heuristic_crossover(sample[0], sample[1])
        sample[3] = Mutation.mutate(sample[3], self.mutation_chance, self.mutation_strength)

        return sample

    def set_unit_factory(self, unit_factory: Callable[[int | float], tuple[int | float, ...]]) -> None:
        self.generate_unit = unit_factory

    def set_player_factory(self, player_factory: Callable[[tuple[int | float, ...], Color], Player]) -> None:
        self.create_player = player_factory

    @staticmethod
    def _take_sample(population: list[tuple[int | float, ...]]) -> list[tuple[int | float, ...]]:
        return [population.pop(random.randint(0, len(population) - 1)) for _ in range(4)]


class AdvancedGeneticAlgorithm:

    def __init__(self, population_size: int, parameter_noise: int | float,
                 iterations: int, tournaments: int, tournament_length: int,
                 mutation_chance: float, mutation_strength: int | float, early_stop_patience: int) -> None:
        self.population_size = population_size
        self.parameter_noise = parameter_noise

        self.iterations = iterations
        self.current_iteration = -1

        self.tournaments = tournaments
        self.tournament_length = tournament_length

        self.mutation_chance = mutation_chance
        self.mutation_strength = mutation_strength

        self.early_stop_patience = early_stop_patience
        self.early_stop_count = 0

        self.population = []
        self.winners = set()

        self.generate_unit: Callable[[int | float], tuple[int | float, ...]] | None = None
        self.create_player: Callable[[tuple[int | float, ...], Color], Player] | None = None

    def run(self) -> None:
        if self.generate_unit is None or self.create_player is None:
            raise ValueError('Unit and player factory functions must be set!')

        self._generate_population()
        self.winners.clear()

        for iteration in range(1, self.iterations + 1):
            self.current_iteration = iteration
            print(f'Iteration {iteration}...')

            for tournament in range(1, self.tournaments + 1):
                print(f'\tTournament {tournament}...')

                while True:
                    sample = self._take_sample(self.population)
                    new_sample = self._run_tournament(sample)
                    if new_sample is None:
                        self.population.extend(sample)
                    else:
                        self.population.extend(new_sample)
                        break

                self.winners.add(new_sample[0])

                print(f'\t\tFinished tournament!')

            count = 0
            for winner in list(self.winners):
                if winner not in self.population:
                    self.winners.remove(winner)
                    count += 1
                else:
                    winner.update(iteration)

            print(f'Removed {count} previous winners from population, {len(self.winners)} winners remaining')

            best_unit = max(self.winners, key=lambda unit: unit.tournaments_won)
            print(f'Best unit in iteration {iteration} by total tournaments won:')
            print(f'\t{best_unit.parameters}')
            print(f'\tCreated in iteration {best_unit.first_iteration}')
            print(f'\tWon {best_unit.tournaments_won} tournaments')
            print(f'\tWon {best_unit.tournaments_won_per_iteration} tournaments per iteration')

            best_unit = max(self.winners, key=lambda unit: unit.tournaments_won_per_iteration)
            print(f'Best unit in iteration {iteration} by tournaments won per iteration:')
            print(f'\t{best_unit.parameters}')
            print(f'\tCreated in iteration {best_unit.first_iteration}')
            print(f'\tWon {best_unit.tournaments_won} tournaments')
            print(f'\tWon {best_unit.tournaments_won_per_iteration} tournaments per iteration')

            print()

            if count == 0:
                self.early_stop_count += 1
            elif self.early_stop_count > 0:
                self.early_stop_count = 0

            if 0 < self.early_stop_patience <= self.early_stop_count:
                break

        for unit in self.population:
            unit.retire(self.current_iteration)

    def _generate_population(self) -> None:
        self.population.clear()

        for _ in range(self.population_size - 1):
            self.population.append(Unit(self.generate_unit(self.parameter_noise)))

        self.population.append(Unit(self.generate_unit(0)))

    def _run_tournament(self, sample: list[Unit]) -> list[Unit] | None:
        colors = [c for c in Color]
        players = {colors[i]: self.create_player(sample[i].parameters, colors[i]) for i in range(len(colors))}

        wins, _, _ = play_batch(self.tournament_length, list(players.values()), quiet=True)

        winners = list(wins.keys())
        if len(winners) < len(sample):
            for color in colors:
                if color not in winners:
                    wins[color] = 0

        print(f'\t\tWin distribution: {wins}')

        finished_games = sum(wins.values())
        if finished_games < self.tournament_length:
            print(f'\t\tNot all games were finished: {finished_games}/{self.tournament_length}')
        if finished_games < self.tournament_length / 2:
            print(f'\t\tRestarting tournament with new sample...')
            return None

        sorted_colors = sorted(wins, key=wins.get, reverse=True)

        sample = [sample[colors.index(color)] for color in sorted_colors]

        self._update_tournament_stats(sample, sorted_colors, wins)

        child = Crossover.arithmetic_crossover(sample[0].parameters, sample[1].parameters)
        child = Mutation.mutate(child, self.mutation_chance, self.mutation_strength)
        sample[2] = Unit(child, self.current_iteration)

        child = Crossover.heuristic_crossover(sample[0].parameters, sample[1].parameters)
        child = Mutation.mutate(child, self.mutation_chance, self.mutation_strength)
        sample[3] = Unit(child, self.current_iteration)

        return sample

    def _update_tournament_stats(self, sample: list[Unit], sorted_colors: list[Color], wins: dict[Color, int]):
        sample[0].tournaments_played += 1
        sample[0].games_won += wins[sorted_colors[0]]
        sample[0].tournaments_won += 1

        if wins[sorted_colors[0]] > self.tournament_length / 2:
            sample[0].tournaments_dominated += 1
            print(f'\t\tPlayer {sorted_colors[0]} dominated the tournament:')
            print(f'\t\t\t{sample[0].parameters}')

        sample[1].tournaments_played += 1
        sample[1].games_won += wins[sorted_colors[0]]

        sample[2].tournaments_played += 1
        sample[2].games_won += wins[sorted_colors[0]]
        sample[2].retire(self.current_iteration)

        sample[3].tournaments_played += 1
        sample[3].games_won += wins[sorted_colors[0]]
        sample[3].retire(self.current_iteration)

    def set_unit_factory(self, unit_factory: Callable[[int | float], tuple[int | float, ...]]) -> None:
        self.generate_unit = unit_factory

    def set_player_factory(self, player_factory: Callable[[tuple[int | float, ...], Color], Player]) -> None:
        self.create_player = player_factory

    @staticmethod
    def _take_sample(population: list[Unit]) -> list[Unit]:
        return [population.pop(random.randint(0, len(population) - 1)) for _ in range(4)]
