import random


class Crossover:

    @staticmethod
    def arithmetic_crossover(unit1: tuple[int | float, ...], unit2: tuple[int | float, ...]) -> tuple[int | float, ...]:
        child = []

        for i in range(len(unit1)):
            alpha = random.uniform(0, 1)
            value = alpha * unit1[i] + (1 - alpha) * unit2[i]
            if isinstance(unit1[i], int):
                value = int(value)
            child.append(value)

        return tuple(child)

    @staticmethod
    def heuristic_crossover(unit1: tuple[int | float, ...], unit2: tuple[int | float, ...]) -> tuple[int | float, ...]:
        child = []

        for i in range(len(unit1)):
            alpha = random.uniform(0, 1)
            value = alpha * (unit1[i] - unit2[i]) + unit1[i]
            if isinstance(unit1[i], int):
                value = int(value)
            child.append(value)

        return tuple(child)
