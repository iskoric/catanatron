import random


class Mutation:

    @staticmethod
    def mutate(unit: tuple[int | float, ...], mutation_chance: float, mutation_strength: int | float)\
            -> tuple[int | float, ...]:
        unit = list(unit)

        for i in range(len(unit)):
            if random.random() < mutation_chance:
                value = random.gauss(0, mutation_strength)
                if isinstance(unit[i], int):
                    value = int(value)
                unit[i] += value

        return tuple(unit)
