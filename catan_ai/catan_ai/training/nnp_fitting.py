import json
import os
import pickle
from argparse import Namespace, ArgumentParser
from datetime import datetime

import numpy as np
import tensorflow as tf
from tensorflow.python.keras.callbacks import EarlyStopping, History
from tensorflow.python.keras.optimizer_v2.adam import Adam
from tensorflow.python.keras.optimizer_v2.learning_rate_schedule import ExponentialDecay

from catan_ai import NeuralNetworkPlayer, ValueFunctionPlayer
from catan_ai.players.util import PlayerType, Value, Resource, expense_costs
from catanatron import Color, Game
from catanatron.game import TURNS_LIMIT

SCRIPT_DIR = f'{os.path.dirname(__file__)}/'
SCRIPT_NAME = os.path.splitext(os.path.basename(__file__))[0]

RESULT_DIR = SCRIPT_DIR + f'../../resources/{SCRIPT_NAME}/'
PARAMETERS_DIR = SCRIPT_DIR + f'../../resources/parameters/'


def parse_arguments() -> Namespace:
    parser = ArgumentParser(description='Genetic algorithm configuration')

    parser.add_argument(
        '-l', '--layers',
        help='Hidden layer sizes, e.g. [-l H1_SIZE H2_SIZE ...]',
        nargs='+', type=int, default=[64, 32]
    )

    parser.add_argument(
        '-c', '--create-dataset', help='Create new dataset',
        type=bool, default=False,
        nargs='?', const=True
    )
    parser.add_argument('-s', '--dataset-size', help='Dataset size when creating new dataset', type=int, default=100000)

    parser.add_argument('-b', '--batch_size', help='Batch size', type=int, default=100)
    parser.add_argument('-e', '--epochs', help='Number of epochs to run', type=int, default=50)

    parser.add_argument('-r', '--learning_rate', help='Optimizer learning rate', type=float, default=0.01)
    parser.add_argument('-d', '--dropout', help='Neuron dropout probability', type=float, default=0.2)

    parser.add_argument(
        '-p', '--early-stop-patience',
        help='Number of epochs without improvement required before stopping the algorithm '
             '(0 or less disables early stopping)',
        type=int, default=3
    )

    return parser.parse_args()


def calculate_value(x: np.ndarray) -> float:
    value = 0
    player = ValueFunctionPlayer(Color.RED, PlayerType.AGGRESSIVE, f'{PARAMETERS_DIR}vfp.txt')

    value += x[0] * player.value_multipliers[Value.VP]
    value += x[1] * player.value_multipliers[Value.CITIES]
    value += x[2] * player.value_multipliers[Value.SETTLEMENTS]
    value += x[3] * player.value_multipliers[Value.ROAD_LENGTH]
    value += x[4] * player.value_multipliers[Value.KNIGHTS]
    value += x[5] * player.value_multipliers[Value.DEV_CARDS]

    production = {
        Resource.BRICK.name: x[6],
        Resource.ORE.name: x[7],
        Resource.SHEEP.name: x[8],
        Resource.WHEAT.name: x[9],
        Resource.WOOD.name: x[10],
    }

    for resource, multiplier in player.resource_multipliers.items():
        value += production[resource.name] * multiplier
    resource_types = sum(map(lambda r: 1 if r > 0 else 0, production.values()))
    value += resource_types * player.value_multipliers[Value.RESOURCE_TYPES]

    ports = 0
    if x[11] > 0:
        ports = sum(production.values()) / len(production.values())
    if x[12] > 0:
        ports += production[Resource.BRICK.name]
    if x[13] > 0:
        ports += production[Resource.ORE.name]
    if x[14] > 0:
        ports += production[Resource.SHEEP.name]
    if x[15] > 0:
        ports += production[Resource.WHEAT.name]
    if x[16] > 0:
        ports += production[Resource.WOOD.name]
    value += ports * player.value_multipliers[Value.PORT_VALUE]

    value += x[17] * player.value_multipliers[Value.BUILDABLE_NODES]
    value += x[18] * player.value_multipliers[Value.BUILDABLE_EDGES]

    resource_count = {
        Resource.BRICK.name: x[19],
        Resource.ORE.name: x[20],
        Resource.SHEEP.name: x[21],
        Resource.WHEAT.name: x[22],
        Resource.WOOD.name: x[23],
    }

    for expense, cost in expense_costs.items():
        current_value = 0
        for resource in cost.keys():
            num_resource = resource_count[resource.name]
            current_value += min(1, num_resource / cost[resource])
        value += current_value * player.expense_multipliers[expense]

    over_discard_limit = max(0, sum(resource_count.values()) - 7)
    value += over_discard_limit * player.value_multipliers[Value.DISCARD_PENALTY]

    value += sum(x[24:27]) * player.value_multipliers[Value.ENEMY_VP]

    for i in range(3):
        production = {
            Resource.BRICK.name: x[27 + i * 5],
            Resource.ORE.name: x[27 + i * 5 + 1],
            Resource.SHEEP.name: x[27 + i * 5 + 2],
            Resource.WHEAT.name: x[27 + i * 5 + 3],
            Resource.WOOD.name: x[27 + i * 5 + 4],
        }

        value += sum(production.values()) * player.value_multipliers[Value.ENEMY_RESOURCE_PRODUCTION]
        enemy_resource_types = sum(map(lambda r: 1 if r > 0 else 0, production.values()))
        value += enemy_resource_types * player.value_multipliers[Value.ENEMY_RESOURCE_TYPES]

    value += sum(x[42:45]) * player.value_multipliers[Value.ENEMY_BUILDABLE_NODES]
    value += sum(x[45:48]) * player.value_multipliers[Value.ENEMY_BUILDABLE_EDGES]

    return value


def create_random_dataset(dataset_size: int):
    print(f'Creating dataset of {dataset_size} examples...')

    x = []
    y = []

    for _ in range(dataset_size):
        x.append(np.random.randint(-100, 100, 48))
        y.append(np.array([calculate_value(x[-1])]))

    x = np.array(x)
    y = np.array(y)

    print('Dataset created!\n')

    print('Saving dataset to pickle file...')
    with open(f'{RESULT_DIR}dataset_{dataset_size}.pickle', 'wb') as file:
        pickle.dump((x, y), file)
        print('Dataset saved!\n')


def create_dataset(dataset_size: int):
    print(f'Creating dataset of {dataset_size} examples...')

    x = []
    y = []

    while len(y) < dataset_size:
        players = [
            ValueFunctionPlayer(Color.RED, PlayerType.AGGRESSIVE, f'{PARAMETERS_DIR}vfp.txt'),
            ValueFunctionPlayer(Color.BLUE, PlayerType.AGGRESSIVE, f'{PARAMETERS_DIR}vfp.txt'),
            ValueFunctionPlayer(Color.ORANGE, PlayerType.AGGRESSIVE, f'{PARAMETERS_DIR}vfp.txt'),
            ValueFunctionPlayer(Color.WHITE, PlayerType.AGGRESSIVE, f'{PARAMETERS_DIR}vfp.txt')
        ]
        # --- RANDOMIZE NUMBER OF PLAYERS ---
        # if random.uniform(0, 1) <= 0.75:
        #     players.append(
        #         ValueFunctionPlayer(Color.ORANGE, PlayerType.AGGRESSIVE, f'{PARAMETERS_DIR}vfp.txt')
        #     )
        #     if random.uniform(0, 1) <= 0.75:
        #         players.append(
        #             ValueFunctionPlayer(Color.WHITE, PlayerType.AGGRESSIVE, f'{PARAMETERS_DIR}vfp.txt')
        #         )

        game = Game(players)

        while len(y) < dataset_size and game.winning_color() is None and game.state.num_turns < TURNS_LIMIT:
            player: ValueFunctionPlayer = game.state.current_player()
            playable_actions = game.state.playable_actions

            for action in playable_actions:
                if len(y) >= dataset_size:
                    break

                game_copy = game.copy()
                game_copy.execute(action)

                state = game_copy.state

                state_input = NeuralNetworkPlayer.create_input(state, player.color)
                x.append(state_input)

                state_value = np.array([player.calculate_value(state)])
                y.append(state_value)

            game.play_tick()

    x = np.array(x)
    y = np.array(y)

    print('Dataset created!\n')

    print('Saving dataset to pickle file...')
    with open(f'{RESULT_DIR}dataset_{dataset_size}.pickle', 'wb') as file:
        pickle.dump((x, y), file)
        print('Dataset saved!\n')


def load_dataset(dataset_size: int):
    with open(f'{RESULT_DIR}dataset_{dataset_size}.pickle', 'rb') as file:
        return pickle.load(file)


def write_history(history: History):
    time = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
    with open(f'{RESULT_DIR}history_{time}.txt', 'w') as file:
        json.dump(history, file, indent=4)


def write_parameters(parameters: tuple[float], mae: int, layers: list[int],):
    with open(f'{PARAMETERS_DIR}nnp_fit_{mae}_{"-".join(map(str, layers))}.txt', 'w') as file:
        file.write(str(parameters))


def main():
    args = parse_arguments()

    if not os.path.isfile(f'{RESULT_DIR}dataset_{args.dataset_size}.pickle') or args.create_dataset:
        create_random_dataset(args.dataset_size)

    print('Loading dataset from pickle file...')
    x, y = load_dataset(args.dataset_size)
    dataset = tf.data.Dataset.from_tensor_slices((x, y))
    print(f'Dataset of {len(dataset)} examples loaded!\n')

    print(f'Splitting dataset...')
    dataset.shuffle(len(dataset))
    train = dataset.take(int(0.8 * len(dataset)))
    test = dataset.skip(int(0.8 * len(dataset))).batch(args.batch_size).prefetch(2)
    valid = train.skip(int(0.6 * len(dataset))).batch(args.batch_size).prefetch(2)
    train = train.take(int(0.6 * len(dataset))).batch(args.batch_size).prefetch(2)
    print(f'Train/valid/test split (60%/20%/20%): '
          f'{len(train) * args.batch_size}/{len(valid) * args.batch_size}/{len(test) * args.batch_size}\n')

    player = NeuralNetworkPlayer(Color.RED, tuple(args.layers))

    print(f'Compiling tensorflow model...')
    model = player.create_model(args.dropout)
    learning_rate = ExponentialDecay(args.learning_rate, decay_steps=len(train), decay_rate=0.5, staircase=True)
    optimizer = Adam(learning_rate=learning_rate)
    model.compile(optimizer=optimizer, loss='mean_squared_error', metrics=['mae'])
    print(f'Input Shape: (None, {NeuralNetworkPlayer.input_size})')
    model.summary()
    print()

    print(f'Fitting tensorflow model...\n')
    callbacks = []
    if args.early_stop_patience > 0:
        callbacks.append(EarlyStopping(monitor='val_mae', patience=args.early_stop_patience, min_delta=0.5))
    history = model.fit(
        train, epochs=args.epochs, validation_data=valid, callbacks=callbacks,
        use_multiprocessing=True, workers=8
    )

    print(f'\nEvaluating tensorflow model...\n')
    results = model.evaluate(test, use_multiprocessing=True, workers=8, return_dict=True)

    player.weights = model.get_weights()
    write_parameters(player.get_parameters(), int(results['mae']), args.layers)
    write_history(history.history)

    print('\nDone!\n')


if __name__ == '__main__':
    main()

