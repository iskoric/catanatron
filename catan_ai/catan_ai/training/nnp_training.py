import json
import os
import pickle
import random
from argparse import Namespace, ArgumentParser

from catan_ai.training.ga import AdvancedGeneticAlgorithm
from catan_ai.training.ga import Unit
from catanatron import Color
from catan_ai import NeuralNetworkPlayer

SCRIPT_DIR = f'{os.path.dirname(__file__)}/'
SCRIPT_NAME = os.path.splitext(os.path.basename(__file__))[0]

RESULT_DIR = SCRIPT_DIR + f'../../resources/{SCRIPT_NAME}/'
PARAMETERS_DIR = SCRIPT_DIR + f'../../resources/parameters/'


def parse_arguments() -> Namespace:
    parser = ArgumentParser(description='Genetic algorithm configuration')

    parser.add_argument(
        '-l', '--layers',
        help='Hidden layer sizes, e.g. [-l H1_SIZE H2_SIZE ...]',
        nargs='+', type=int, default=[8]
    )

    parser.add_argument('-f', '--parameter-filename', help='Starting parameter filename', type=str)

    parser.add_argument('-p', '--population', help='Population size', type=int, default=60)
    parser.add_argument(
        '-n', '--noise',
        help='Standard deviation of parameters for initial population (Gaussian)',
        type=int, default=40
    )

    parser.add_argument('-i', '--iterations', help='Number of iterations to run', type=int, default=500)

    parser.add_argument('-t', '--tournaments', help='Number of tournaments per iteration', type=int, default=20)
    parser.add_argument('-g', '--games', help='Number of games per tournament', type=int, default=20)

    parser.add_argument('-c', '--mutation-chance', help='Mutation chance', type=float, default=0.3)
    parser.add_argument(
        '-s', '--mutation-strength',
        help='Standard deviation for parameter mutation (Gaussian)',
        type=int, default=20
    )

    parser.add_argument(
        '-e', '--early-stop-patience',
        help='Number of iterations without improvement required before stopping the algorithm '
             '(0 or less disables early stopping)',
        type=int, default=0
    )

    return parser.parse_args()


def write_results(winners: set[Unit]):
    os.makedirs(RESULT_DIR, exist_ok=True)

    winners = sorted(
        winners,
        key=lambda unit: (unit.tournaments_won_per_iteration, unit.games_won_per_iteration),
        reverse=True
    )
    with open(f'{RESULT_DIR}winners.json', 'w') as file:
        json.dump([winner.__dict__ for winner in winners], file, indent=4)
    with open(f'{RESULT_DIR}winners.pickle', 'wb') as file:
        pickle.dump(winners, file)

    player = max(winners, key=lambda unit: (unit.tournaments_won, unit.games_won))
    with open(f'{RESULT_DIR}most_tournaments_won.json', 'w') as file:
        json.dump(player.__dict__, file, indent=4)
    with open(f'{RESULT_DIR}most_tournaments_won.pickle', 'wb') as file:
        pickle.dump(player, file)

    player = winners[0]
    with open(f'{RESULT_DIR}most_tournaments_won_per_iteration.json', 'w') as file:
        json.dump(player.__dict__, file, indent=4)
    with open(f'{RESULT_DIR}most_tournaments_won_per_iteration.pickle', 'wb') as file:
        pickle.dump(player, file)


def main():
    args = parse_arguments()

    ga = AdvancedGeneticAlgorithm(
        population_size=args.population,
        parameter_noise=args.noise,
        iterations=args.iterations,
        tournaments=args.tournaments,
        tournament_length=args.games,
        mutation_chance=args.mutation_chance,
        mutation_strength=args.mutation_strength,
        early_stop_patience=args.early_stop_patience
    )

    def generate_nnp_params(noise: float) -> tuple[float, ...]:
        param_filepath = f'{PARAMETERS_DIR}{args.parameter_filename}' if args.parameter_filename is not None else None
        player = NeuralNetworkPlayer(Color.RED, tuple(args.layers), param_filepath)
        parameters = list(player.get_parameters())

        if noise > 0:
            for i in range(len(parameters)):
                parameters[i] += random.gauss(0, noise)

        return tuple(parameters)

    def create_nn_player(parameters: tuple[float, ...], color: Color) -> NeuralNetworkPlayer:
        player = NeuralNetworkPlayer(color, tuple(args.layers))
        player.set_parameters(parameters)
        return player

    ga.set_unit_factory(generate_nnp_params)
    ga.set_player_factory(create_nn_player)

    ga.run()

    write_results(ga.winners)


if __name__ == '__main__':
    main()
