import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='catan_ai',
    version='1.0.0',
    author='Ivan Skorić',
    author_email='ivan.skoric@fer.hr',
    description='AI agents for Catanatron - a Python implementation of the board game Settlers of Catan',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/iskoric/catanatron.git',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.8',
    entry_points={
        'console_scripts': [
            'play-batch=catan_ai.game.terminal_runner:main',
            'play-ui=catan_ai.game.browser_runner:main',
            'train-vf=catan_ai.training.vfp_training:main'
        ]
    },
    install_requires=[
        'catanatron==3.1.2',
        'tensorflow==2.8.0',
        'keras==2.8.0',
        'numpy==1.22.1',
        'rich==11.1.0',
        'protobuf==3.20.*'
    ]
)
