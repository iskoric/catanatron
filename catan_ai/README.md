# Catan AI

_Ivan Skorić_\
_University of Zagreb_\
_Faculty of Electrical Engineering and Computing_\
_June, 2022_

AI agents for Bryan Collazo's [Catanatron](https://github.com/bcollazo/catanatron) - a Python implementation of the board game
[Settlers of Catan](https://www.catan.com/)

For more in-depth information about the original framework,
read the main [`catanatron/README.md`](../README.md)

## Getting started
The project requires an environment with Python version 3.8 or higher

1. Installing the requirements
    ```shell
    # From root 'catanatron' folder
    pip install -r dev-requirements.txt
    pip install -r requirements.txt
    ```

    At this point, running batch simulations should be possible
    using the `terminal_runner.py` script from the `catan_ai` module,
    or the alias:
    ```shell
    play-batch  # Use --help to view the manual
    ```

2. Starting the web application\
    Using **Docker Compose**:
    ```shell
    docker-compose up -d --build  # The --build option is only required once
    ```
    Or manually (requires **Docker Compose**, **Node.js** and **npm**):
    ```shell
    # From 'catanatron/ui'
    npm install
    npm start
    # From 'catanatron/catanatron_server/catanatron_server'
    flask run
    # From root 'catanatron' folder
    docker-compose up -d db
    ```

    Now, the game GUI should also be accessible using
    the `browser_runner.py` script from the `catan_ai` module,
    or the alias:
    ```shell
    play-ui  # Use --help to view the manual
    # HUMAN player is not necessary to view AI-only games
    ```

## Player parameters
When running `play-batch` or `play-ui`, players can be defined
along with their constructor parameters, except player color.
The player code and parameters should be separated by semicolons ( ; ).

### Value Function Player
- **parameter_filepath** (textual file containing parameters formatted as a tuple)
    ```shell
    IS-VF;catan_ai/resources/parameters/vfp.txt
    ```

### Neural Network Player
- **layers** (hidden layer sizes formatted as a tuple)
- **parameter_filepath** (textual file containing parameters formatted as a tuple)
    ```shell
    IS-NN;(32,16);catan_ai/resources/parameters/nnp_32-16.txt
    ```

### Tree Search Player
- **depth** (integer greater or equal to 1)
- **parameter_filepath** (textual file containing parameters formatted as a tuple)
    ```shell
    IS-TS;3;catan_ai/resources/parameters/tsp_2.txt
    ```

### Examples:
```shell
# From root 'catanatron' folder
play-batch -n 100 -p IS-VF IS-NN;(32,16);catan_ai/resources/parameters/nnp_32-16.txt
play-ui -p HUMAN IS-TS;3;catan_ai/resources/parameters/tsp_2.txt
```
